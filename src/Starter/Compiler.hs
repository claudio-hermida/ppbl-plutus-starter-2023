{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

module Starter.Compiler where

import Cardano.Api
import Cardano.Api.Shelley (PlutusScript (..), PlutusScriptV2)
import Codec.Serialise (serialise)
import Data.Aeson
import qualified Data.ByteString.Lazy as LBS
import qualified Data.ByteString.Short as SBS
import qualified Plutus.V1.Ledger.Scripts
import qualified Plutus.V1.Ledger.Value
import qualified Plutus.V2.Ledger.Api
import qualified Plutus.V2.Ledger.Contexts
import qualified PlutusTx
import PlutusTx.Prelude
import Prelude (FilePath, IO)

import qualified Starter.Validator as Starter
import Starter.Minter as Minter
import Starter.Types

writeValidator :: FilePath -> Plutus.V2.Ledger.Api.Validator -> IO (Either (FileError ()) ())
writeValidator file = writeFileTextEnvelope @(PlutusScript PlutusScriptV2) file Nothing . PlutusScriptSerialised . SBS.toShort . LBS.toStrict . serialise . Plutus.V2.Ledger.Api.unValidatorScript

-- One StarterValidator is all we need
starterParams :: StarterParams
starterParams = StarterParams {
    adminTokenPolicyId = "59ec9fccf82280db679b97735d9a4c659ccdab0503ec7cfbfbc1e512",
    version = 1
}

writeStarterScript :: IO (Either (FileError ()) ())
writeStarterScript = writeValidator "output/ppbl-starter-Rvalidator.plutus" $ Starter.validator starterParams

writeMinterScript :: IO (Either (FileError ()) ())
writeMinterScript = writeValidator "output/ppbl-starter-minter.plutus" Minter.validator