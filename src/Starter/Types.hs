{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}

module Starter.Types
  ( StarterParams (..),
    StarterDatum (..),
    StarterAction (..),
    StarterRAction(..),
    StarterTypes,
  )
where

import GHC.Generics (Generic)
import Plutus.Script.Utils.V1.Typed.Scripts.Validators (DatumType, RedeemerType)
import Plutus.Script.Utils.V2.Typed.Scripts (TypedValidator, ValidatorTypes, mkTypedValidator, mkTypedValidatorParam, mkUntypedValidator, validatorScript)
import Plutus.V1.Ledger.Value
import Plutus.V2.Ledger.Api
import Plutus.V2.Ledger.Contexts
import qualified PlutusTx
import PlutusTx.Prelude hiding (Semigroup (..), unless)
import Prelude (Show (..))
import qualified Prelude as Pr

data StarterParams = StarterParams
  { adminTokenPolicyId :: CurrencySymbol,
    version            :: Integer
  }
  deriving (Pr.Eq, Pr.Ord, Show, Generic)

PlutusTx.makeLift ''StarterParams
data StarterRAction = TrueF | FalseF | TrueS | FalseS
    deriving    (Show, Generic, Pr.Eq, Pr.Ord, Pr.Read)

PlutusTx.makeIsDataIndexed ''StarterRAction [('TrueF, 0), ('FalseF, 1) , ('TrueS, 2) , ('FalseS, 3)]
PlutusTx.makeLift ''StarterRAction
type StarterDatum = Integer
type StarterAction = Integer

data StarterTypes

instance ValidatorTypes StarterTypes where
  type DatumType StarterTypes = StarterDatum
  type RedeemerType StarterTypes = StarterRAction