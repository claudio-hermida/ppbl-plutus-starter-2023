{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE RecordWildCards #-}

module Starter.Validator (validator, starterValidatorHash) where

import Ledger (scriptHashAddress)
import qualified Ledger.Ada as Ada
import Plutus.Script.Utils.V1.Typed.Scripts.Validators (DatumType, RedeemerType)
import Plutus.Script.Utils.V2.Typed.Scripts (TypedValidator, ValidatorTypes, mkTypedValidator, mkTypedValidatorParam, mkUntypedValidator, validatorHash, validatorScript)
import Plutus.V1.Ledger.Value
import Plutus.V2.Ledger.Api
import Plutus.V2.Ledger.Contexts
import PlutusTx
import PlutusTx.Prelude hiding (Semigroup (..), unless)
import Prelude (Show (..))
import qualified Prelude as Pr

import Starter.Types

{-# INLINEABLE starterValidator #-}
starterValidator :: StarterParams -> StarterDatum -> StarterRAction -> ScriptContext -> Bool
starterValidator param datum action ctx@ScriptContext{ scriptContextTxInfo = info@TxInfo{..}} = 
  case action of
    TrueF   -> True
    FalseF  -> False
    TrueS   -> True
    FalseS  -> False
    _       -> False


typedValidator :: StarterParams -> TypedValidator StarterTypes
typedValidator tp = go tp
  where
    go =
      mkTypedValidatorParam @StarterTypes
        $$(PlutusTx.compile [||starterValidator||])
        $$(PlutusTx.compile [||wrap||])
    wrap = mkUntypedValidator

validator :: StarterParams -> Validator
validator = validatorScript . typedValidator

starterValidatorHash :: StarterParams -> ValidatorHash
starterValidatorHash = validatorHash . typedValidator
