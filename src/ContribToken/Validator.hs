{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE RecordWildCards #-}

module ContribToken.Validator (validator) where

import Ledger (scriptHashAddress)
import qualified Ledger.Ada as Ada
import Plutus.Script.Utils.V1.Typed.Scripts.Validators (DatumType, RedeemerType)
import Plutus.Script.Utils.V2.Typed.Scripts (TypedValidator, ValidatorTypes, mkTypedValidator, mkTypedValidatorParam, mkUntypedValidator, validatorHash, validatorScript)
import Plutus.V1.Ledger.Value
import Plutus.V2.Ledger.Api
import Plutus.V2.Ledger.Contexts
import PlutusTx
import PlutusTx.Builtins
import PlutusTx.Prelude hiding (Semigroup (..), unless)
import Prelude (Show (..))
import qualified Prelude as Pr

import ContribToken.Types

{-# INLINEABLE contributorReferenceValidator #-}
contributorReferenceValidator :: ContribReferenceParams -> ContributorReferenceDatum -> ContributorReferenceAction -> ScriptContext -> Bool
contributorReferenceValidator param datum action ctx@ScriptContext{ scriptContextTxInfo = info@TxInfo{..}} =
  let
    inVals :: [CurrencySymbol]
    inVals = symbols $ valueSpent info

    inputHasAdminToken :: Bool
    inputHasAdminToken = adminTokenPolicyId param `elem` inVals

    ownOutput :: Maybe TxOut
    ownOutput = case getContinuingOutputs ctx of
      [o] -> Just o
      _   -> Nothing

    newReferenceDatum :: Maybe ContributorReferenceDatum
    newReferenceDatum = case ownOutput of
      Nothing -> Nothing
      Just o -> case txOutDatum o of
        ( OutputDatum ( Datum d)) -> case fromBuiltinData d of
          Nothing   -> Nothing
          Just mrd  -> Just $ unsafeFromBuiltinData @ContributorReferenceDatum mrd
        _                         -> Nothing

    allInputValues :: Value
    allInputValues = valueSpent info

  in case action of
    UpdateCompletedModules ->
      let
        checkUpdatedDatum :: Bool
        checkUpdatedDatum = case newReferenceDatum of
          Nothing -> False
          Just d -> luckyNumber datum == luckyNumber d

      in
        traceIfFalse "Admin Token Required"    inputHasAdminToken &&
        traceIfFalse "New datum is not valid"  checkUpdatedDatum

    (UpdateNumber num) ->
      let
        requiredDatum = ContributorReferenceDatum {
          luckyNumber = num,
          completedModules = completedModules datum
        }

        checkUpdatedDatum :: Bool
        checkUpdatedDatum = case newReferenceDatum of
          Nothing -> False
          Just d -> d == requiredDatum


        inputHasContributorRefTokenPair :: Bool
        inputHasContributorRefTokenPair = case [ tn | (cs, tn, _) <- flattenValue allInputValues, cs == contributorTokenPolicyId param ] of
            [a,t] -> dropByteString 3 (unTokenName a) == dropByteString 3 (unTokenName t)
            _     -> False

      in
        traceIfFalse "Contributor and reference tokens do not match"    inputHasContributorRefTokenPair &&
        traceIfFalse "New datum is not valid"                           checkUpdatedDatum

    RemoveContributor -> traceIfFalse "Admin Token Required" inputHasAdminToken

typedValidator :: ContribReferenceParams -> TypedValidator ContributorReferenceTypes
typedValidator tp = go tp
  where
    go =
      mkTypedValidatorParam @ContributorReferenceTypes
        $$(PlutusTx.compile [||contributorReferenceValidator||])
        $$(PlutusTx.compile [||wrap||])
    wrap = mkUntypedValidator

validator :: ContribReferenceParams -> Validator
validator = validatorScript . typedValidator

contributorReferenceValidatorHash :: ContribReferenceParams -> ValidatorHash
contributorReferenceValidatorHash = validatorHash . typedValidator
