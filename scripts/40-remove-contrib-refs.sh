#!/usr/bin/env bash

# Endpoint: TreasuryValidator -> UpdateNFTList

# Proceed with caution, this validator is yet working as expected.
# See docs:

source 00-project-variables.sh
source getTxFunc.sh

rm build-tx.sh
touch build-tx.sh

echo "cardano-cli transaction build \\" > build-tx.sh
echo "--babbage-era \\" >> build-tx.sh
echo "$NETWORK \\" >> build-tx.sh

echo ""
echo "--------------------------------------------------------------------------------------------"
echo "Choose Fees UTxO:"
chooseWalletUTxO $ADMIN_WALLET
TX_IN=${SELECTED_UTXO}
echo "--tx-in $TX_IN \\" >> build-tx.sh
echo "--tx-in-collateral $TX_IN \\" >> build-tx.sh
echo ""
echo "--------------------------------------------------------------------------------------------"
echo "Choose UTxO with OUOD Admin Token:"
echo ""
chooseWalletUTxO $ADMIN_WALLET
ADMIN_UTXO=${SELECTED_UTXO}
ADMIN_ASSET=${SELECTED_UTXO_ASSET}
echo "--tx-in $ADMIN_UTXO \\" >> build-tx.sh
echo ""
echo "--------------------------------------------------------------------------------------------"

echo "Choose Reference UTxO:"

chooseContractUTxO $CONTRIBUTOR_REFERENCE_ADDRESS
REFERENCE_UTXO=${SELECTED_UTXO}
REFERENCE_ASSET=${SELECTED_UTXO_ASSET}
echo "--tx-in $REFERENCE_UTXO \\" >> build-tx.sh
echo "--spending-tx-in-reference $CONTRIBUTOR_REF_REF_UTXO \\" >> build-tx.sh
echo "--spending-plutus-script-v2 \\" >> build-tx.sh
echo "--spending-reference-tx-in-inline-datum-present \\" >> build-tx.sh
echo "--spending-reference-tx-in-redeemer-file $RemoveContributorRedeemerFile \\" >> build-tx.sh
echo "--tx-out $ADMIN_ADDRESS+\"1500000 + 1 $REFERENCE_ASSET\" \\" >> build-tx.sh
echo "--tx-out $ADMIN_ADDRESS+\"1500000 + 1 $ADMIN_ASSET\" \\" >> build-tx.sh
echo "--change-address $ADMIN_ADDRESS \\" >> build-tx.sh
echo "--protocol-params-file protocol.json \\" >> build-tx.sh
echo "--out-file remove-contrib-ref-token.draft" >> build-tx.sh

cat build-tx.sh
. build-tx.sh

cardano-cli transaction sign \
--tx-body-file remove-contrib-ref-token.draft \
$NETWORK \
--signing-key-file $ADMIN_SKEY \
--out-file remove-contrib-ref-token.signed

cardano-cli transaction submit \
$NETWORK \
--tx-file remove-contrib-ref-token.signed

rm build-tx.sh
