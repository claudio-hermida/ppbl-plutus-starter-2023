. 00-project-variables.sh
source getTxFunc.sh

# This script would need to be rewritten to use plutus minting script.

rm build-tx.sh
touch build-tx.sh

echo "cardano-cli transaction build \\" > build-tx.sh
echo "--babbage-era \\" >> build-tx.sh
echo "$NETWORK \\" >> build-tx.sh

read -p "Enter Wallet Directory: " WALLET_DIR
export CURRENT_ADDRESS=$(cat $WALLET_DIR/payment.addr)
export CURRENT_SKEY="$WALLET_DIR/payment.skey"

echo "Choose Fees UTxO:"
chooseWalletUTxO $WALLET_DIR
FEE_UTXO=${SELECTED_UTXO}
echo "--tx-in $FEE_UTXO \\" >> build-tx.sh
echo ""

burnList="-1 "

echo "Burn a Mentor Token? ([no] to quit)"
while read tokenToBurn; do
    if [ "$tokenToBurn" == "no" ]; then
        burnList=${burnList::-6}
        break
    else
        echo "--------------------------------------------------------------------------------------------"
        echo "Choose UTxO with token to burn:"
        chooseWalletUTxO $WALLET_DIR
        TOKEN_UTXO=${SELECTED_UTXO}
        ASSET_TO_BURN=${SELECTED_UTXO_ASSET}
        burnList="$burnList$ASSET_TO_BURN + -1 "
        echo "--tx-in $TOKEN_UTXO \\" >> build-tx.sh
        echo ""
        echo "--------------------------------------------------------------------------------------------"
        echo "Another UTxO? (Type no to stop)"
    fi
done

echo "--mint=\"$burnList\" \\" >> build-tx.sh
# echo "--minting-script-file CHANGE ME \\" >> build-tx.sh
echo "--change-address $CURRENT_ADDRESS \\" >> build-tx.sh
echo "--out-file tx.draft" >> build-tx.sh

cat build-tx.sh
. build-tx.sh

cardano-cli transaction sign \
--tx-body-file tx.draft \
--signing-key-file $CURRENT_SKEY \
--signing-key-file /home/james/hd2/oneuponedown/v2/tokens/MentorTest/policy.skey \
--testnet-magic 1 \
--out-file tx.signed

cardano-cli transaction submit \
--testnet-magic 1 \
--tx-file tx.signed
