#!/usr/bin/env bash

source 00-project-variables.sh
source getTxFunc.sh

rm build-tx.sh
touch build-tx.sh

. writeUpdateCompletedModulesDatumAndRedeemer.sh

echo "cardano-cli transaction build \\" > build-tx.sh
echo "--babbage-era \\" >> build-tx.sh
echo "$NETWORK \\" >> build-tx.sh

echo ""
echo "--------------------------------------------------------------------------------------------"
echo "Choose Fees UTxO:"
chooseWalletUTxO $CONTRIBUTOR_TEST_WALLET
TX_IN=${SELECTED_UTXO}
echo "--tx-in $TX_IN \\" >> build-tx.sh
echo "--tx-in-collateral $TX_IN \\" >> build-tx.sh
echo ""
echo "--------------------------------------------------------------------------------------------"
echo "Choose UTxO with Contributor Token:"
echo ""
chooseWalletUTxO $CONTRIBUTOR_TEST_WALLET
CONTRIBUTOR_TEST_UTXO=${SELECTED_UTXO}
CONTRIBUTOR_TEST_ASSET=${SELECTED_UTXO_ASSET}
echo "--tx-in $CONTRIBUTOR_TEST_UTXO \\" >> build-tx.sh
echo ""
echo "--------------------------------------------------------------------------------------------"

echo "Choose Reference UTxO:"
chooseContractUTxO $CONTRIBUTOR_REFERENCE_ADDRESS
REFERENCE_UTXO=${SELECTED_UTXO}
REFERENCE_ASSET=${SELECTED_UTXO_ASSET}

# Build the transaction

echo "--tx-in $REFERENCE_UTXO \\" >> build-tx.sh
echo "--spending-tx-in-reference $CONTRIBUTOR_REF_REF_UTXO \\" >> build-tx.sh
echo "--spending-plutus-script-v2 \\" >> build-tx.sh
echo "--spending-reference-tx-in-inline-datum-present \\" >> build-tx.sh
echo "--spending-reference-tx-in-redeemer-file UpdateNumber.json \\" >> build-tx.sh
echo "--tx-out $CONTRIBUTOR_REFERENCE_ADDRESS+\"1700000 + 1 $REFERENCE_ASSET\" \\" >> build-tx.sh
echo "--tx-out-inline-datum-file OutgoingContributorDatum.json \\" >> build-tx.sh
echo "--tx-out $CONTRIBUTOR_TEST_ADDRESS+\"1500000 + 1 $CONTRIBUTOR_TEST_ASSET\" \\" >> build-tx.sh
echo "--change-address $CONTRIBUTOR_TEST_ADDRESS \\" >> build-tx.sh
echo "--protocol-params-file protocol.json \\" >> build-tx.sh
echo "--out-file contrib-updates-lucky-number.draft" >> build-tx.sh

cat build-tx.sh
. build-tx.sh

cardano-cli transaction sign \
--tx-body-file contrib-updates-lucky-number.draft \
$NETWORK \
--signing-key-file $CONTRIBUTOR_TEST_SKEY \
--out-file contrib-updates-lucky-number.signed

cardano-cli transaction submit \
$NETWORK \
--tx-file contrib-updates-lucky-number.signed

rm build-tx.sh
mv OutgoingContributorDatum.json datums/OutgoingContributorDatum$LUCKY_NUMBER.json
mv UpdateNumber.json redeemers/UpdateNumber$LUCKY_NUMBER.json