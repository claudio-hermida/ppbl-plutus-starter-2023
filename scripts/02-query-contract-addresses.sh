#!/usr/bin/env bash

# Inspect relevant wallets

source 00-project-variables.sh

echo "-------- OneUpOneDown Plutus ---------------------------------------------------------------"
echo ""

echo "-------- CONTRIBUTOR Reference Contract: --------------------------------------------------------"
echo ""
echo -e "\e[1;32m  address: $CONTRIBUTOR_REFERENCE_ADDRESS"
echo -e "\e[m"
cardano-cli query utxo --testnet-magic 1 --address $CONTRIBUTOR_REFERENCE_ADDRESS
echo ""
echo ""
echo ""
echo ""

echo "-------- Admin Testing Wallet: --------------------------------------------------------"
echo ""
echo -e "\e[1;32m  address: $ADMIN_WALLET"
echo -e "\e[m"
cardano-cli query utxo --testnet-magic 1 --address $ADMIN_ADDRESS
echo ""
echo ""
echo ""
echo ""

echo "-------- Contributor Testing Wallet: --------------------------------------------------------"
echo ""
echo -e "\e[1;32m  address: $CONTRIBUTOR_TEST_WALLET"
echo -e "\e[m"
cardano-cli query utxo --testnet-magic 1 --address $CONTRIBUTOR_TEST_ADDRESS
echo ""
echo ""
echo ""
echo ""


echo "-------- Reference Script Address: ----------------------------------------------------------"
echo ""
echo -e "\e[1;32m  address: $REFERENCE_ADDRESS"
echo -e "\e[m"
cardano-cli query utxo --testnet-magic 1 --address $REFERENCE_ADDRESS
echo ""
echo ""
echo ""
echo ""