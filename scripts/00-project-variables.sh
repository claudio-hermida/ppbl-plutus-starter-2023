source local-secrets.sh
# CARDANO_NODE_SOCKET_PATH
# PROJECTPATH
# WALLETDIR

export NETWORK="--testnet-magic 1"
export VERSION="001"

cardano-cli query tip $NETWORK > tip.json
cardano-cli query protocol-parameters $NETWORK --out-file protocol.json

# -----------------------------------------------------------------------------------------------------
# Reference UTxOs (for PlutusV2 Reference Scripts)
# Keep these current:

export CONTRIBUTOR_REF_REF_UTXO="e4155f5ef706a87611a99c16fc030ad8efa276cb2f59a1332bcee564736eb547#0"

#
# REFERENCE WALLET
#

export REFERENCE_WALLET="$WALLETDIR/ouodReferenceAddress"
export REFERENCE_ADDRESS=$(cat $REFERENCE_WALLET/payment.addr)
export REFERENCE_SKEY="$REFERENCE_WALLET/payment.skey"

# -----------------------------------------------------------------------------------------------------
#
# ADMIN + TOKENS
#

export ADMIN_WALLET="$WALLETDIR/ppbl2023Admin1"
export ADMIN_ADDRESS=$(cat $ADMIN_WALLET/payment.addr)
export ADMIN_SKEY="$ADMIN_WALLET/payment.skey"

export CONTRIBUTOR_TEST_WALLET="$WALLETDIR/ppbl2023Contributor1"
export CONTRIBUTOR_TEST_ADDRESS=$(cat $CONTRIBUTOR_TEST_WALLET/payment.addr)
export CONTRIBUTOR_TEST_SKEY="$CONTRIBUTOR_TEST_WALLET/payment.skey"


export adminTokenPolicyId=15a9a88cf6e6f4e806a853cede246d0430455d4944401b9b71309fca
export contributorTokenPolicyId=05cf1f9c1e4cdcb6702ed2c978d55beff5e178b206b4ec7935d5e056

# ---------------#
#
#    CONTRACTS
#
# ---------------#

# -----------------------------------------------------------------------------------------------------
#
# Mentor Reference Validator
#

# # Path to Plutus Script
CONTRIBUTOR_REFERENCE_PLUTUS_SCRIPT="$PROJECTPATH/output/ppbl-contrib-token-validator-${VERSION}.plutus"
# # Build an address and validator hash for this contract, then export each
cardano-cli address build --payment-script-file $CONTRIBUTOR_REFERENCE_PLUTUS_SCRIPT $NETWORK --out-file contributor-reference.addr
cardano-cli transaction policyid --script-file $CONTRIBUTOR_REFERENCE_PLUTUS_SCRIPT > contributor-reference.vhash
export CONTRIBUTOR_REFERENCE_ADDRESS=$(cat contributor-reference.addr)
export CONTRIBUTOR_REFERENCE_VALIDATOR_HASH=$(cat contributor-reference.vhash)

UpdateCompletedModules=$PROJECTPATH/scripts/redeemers/UpdateCompletedModules.json
RemoveContributorRedeemerFile=""
